#!/bin/bash
echo -e "\nexport PATH=$PATH" >>env.sh
SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
# Set the log file path
log_file=$(pwd)/backup.log
if [ ! -f "$log_file" ]; then
  touch $log_file
fi
(
  crontab -l 2>/dev/null
  echo "0 14 * * SAT $SCRIPT_DIR/dump-mariadb.sh >> $log_file 2>&1"
) | crontab -
