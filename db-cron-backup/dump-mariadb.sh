#!/bin/bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
source $SCRIPT_DIR/env.sh

now=$(date +"%Y%m%d-%H%M%S")
dump_file_name="specify-dump-$now.sql"

tmp_dump=$(mktemp)

docker exec all-in-one-mariadb-1 bash -c "mariadb-dump --user=$DB_USER --password=$DB_PASSWORD --single-transaction --all-databases" >$tmp_dump

token="$(curl -s -X POST \
  -H "Content-Type: application/json" \
  -d '{
    "auth": {
      "identity": {
        "methods": ["application_credential"],
        "application_credential": {
          "id": "'"$AC_ID"'",
          "secret": "'"$AC_SECRET"'"
        }
      }
    }
  }' \
  "https://keystone.rc.nectar.org.au:5000/v3/auth/tokens" \
  -D -)"

os_token="$(echo "$token" | grep "X-Subject-Token:" | awk '{print $2}' | tr -d '\r')"
project_id=$(echo "$token" | sed '1,/^\s*$/d' | jq -r '.token.project.id // empty')
headers="X-Auth-Token: $os_token"
containerURI="https://object-store.rc.nectar.org.au/v1/AUTH_${project_id}/${CONTAINER_NAME}"
dump_file_url="$containerURI/$dump_file_name"

curl --request PUT --header "$headers" --data-binary @"$tmp_dump" "$dump_file_url"
